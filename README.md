# README #

This is jQuery plugin for creating loader based on CSS only

For using firstly get elememt using jQuery, then launch method `$(...).loader();`. It will creating two div containers into selected container:

1. First for overlaying all content into parent container.
2. Second is a spinner container which animated using CSS rotate.

For removing loader use `$(...).loader(false);`.

### Default configs ###
In this plugin used these defaults settings:

* `backColor: 'rgba(255,255,255,.5)'` - background color for overlaying container;
* `zIndex: 999` - z-index for overlaying container;
* `thickness: '3px'` - thickness for spinner border;
* `size: '100px'` - spinner width and height;
* `color: '#4C6F98'` - spinner border color;
* `animationSpeed: '800ms'` - spinner animation.

### Used styles ###
For animation used this CSS keyframes:
``` css
    @keyframes spinner {
        100% {
            -webkit-transform: rotate(360deg);
            transform:rotate(360deg);
        }
    }
```

---

For overlaying container with class - loader-container:
``` css
    .loader-container {
        width: 100%;
        height: 100%;
        position: absolute;
        top: 0;
        left: 0;
        background-color: rgba(255,255,255,.5);
        z-index: 999;
    }
```

---

For spinner container with class - loader-spinner:
``` css
    .loader-spinner {
        border: 3px solid #4C6F98;
        border-right-color: transparent;
        border-bottom-color: transparent;
        border-radius: 50%;
        width: 100px;
        height: 100px;
        margin: 0 auto;
        position: relative;
        top: calc(50% - 50px);
        animation: spinner 800ms linear infinite;
    }
```
