﻿;
/*
    Author: Bojko Evgen
    Date: 28.03.2017
    URL: https://bitbucket.org/bojko-evgen/

    Plugin for add spinner.
    For use firstly get element using jQuery, then launch method loader().
    Get two parametes:
        - boolean - activate or deactivate spinner;
        - object - settings for overridind defaults spinner settings.

    Default settings:
        - backColor:        'rgba(255,255,255,.5)';
        - zIndex:           '999';
        - thickness:        '3px';
        - size:             '100px';
        - color:            '#4C6F98';
        - animationSpeed:   '800ms'.
*/

// Needed for adding style for keyframes
window.firstSpinnerLoad = true;

(function ($) {

    $.fn.loader = function (activate, newSettings) {
        // If no container
        if (!this) {
            return;
        }

        var containerClass  = 'loader-container';
        var spinnerClass    = 'loader-spinner';

        // If loader already added in current container remove it
        this.find('.' + containerClass).remove();
         
        // Remove loader from current container
        if (activate === false) {
            this.find('.' + containerClass).remove();

            return;
        }

        // Default settings with possibility of overriding values with user settings
        var settings = $.extend({
            'backColor':        'rgba(255,255,255,.5)',
            'zIndex':           '999',
            'thickness':        '3px',
            'size':             '100px',
            'color':            '#4C6F98',
            'animationSpeed':   '800ms'
        }, newSettings);

        // Get half spinner size for 
        var halfOfSpinnerSize = +Math.floor(settings.size.replace('px', '') / 2);

        // Set needed position for parent container
        var currentPosition = this.css('position');
        if (currentPosition !== 'relative' || currentPosition !== 'absolute' || currentPosition !== 'fixed') {
            this.css({
                'position': 'relative'
            });
        }

        // Create new element for spinner and set styles to it
        $loader = $('<div class="' + containerClass + '"><div class="' + spinnerClass + '"></div><div>');

        $loader
            .css({
                'width':            '100%',
                'height':           '100%',
                'position':         'absolute',
                'top':              '0',
                'left':             '0',
                'background-color': settings.backColor,
                'z-index':          settings.zIndex
            });

        $loader.find('.' + spinnerClass)
            .css({
                'border':               settings.thickness + ' solid ' + settings.color,
                'border-right-color':   'transparent',
                'border-bottom-color':  'transparent',
                'border-radius':        '50%',
                'width':                settings.size,
                'height':               settings.size,
                'margin':               '0 auto',
                'position':             'relative',
                'top':                  'calc(50% - ' + halfOfSpinnerSize + 'px)',
                'animation':            'spinner ' + settings.animationSpeed + ' linear infinite',
            });

        // If plugin loaded first - add stylesheet with keyframes to head
        if (window.firstSpinnerLoad) {
            $('<style type="text/css"> @keyframes spinner {100% {-webkit-transform: rotate(360deg);transform:rotate(360deg);}} </style>')
                .appendTo('head');
            window.firstSpinnerLoad = false;
        }

        // Add spinner to parent
        this.append($loader);

        return this;
    };

})(jQuery)